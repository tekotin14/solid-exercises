package ocp.not_ocp;

/**
 * Created by grazi on 23.08.16.
 */
public class HospitalManagement {

    public void callUpon (Employee employee) {
        if (employee instanceof Nurse) {
            checkVitalSigns();
            drawBlood();
            cleanPatientArea();
        } else if (employee instanceof Doctor) {
            prescribeMedicine();
            diagnosePatient();
        }
    }

    // Nurses
    private void checkVitalSigns() {
        System.out.println("checking vitals");
    }

    private void drawBlood () {
        System.out.println("drawing blood");
    }

    private void cleanPatientArea() {
        System.out.println("cleaning Patient Area");
    }

    // Doctors
    private void prescribeMedicine () {
        System.out.println("Prescribe Medicine");
    }

    private void diagnosePatient () {
        System.out.println("Diagnosing Patient");
    }


}


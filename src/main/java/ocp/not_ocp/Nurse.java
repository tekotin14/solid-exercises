package ocp.not_ocp;

/**
 * Created by grazi on 23.08.16.
 */
public class Nurse extends Employee {
    public Nurse(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
        System.out.println("Nurse in action...");
    }


}

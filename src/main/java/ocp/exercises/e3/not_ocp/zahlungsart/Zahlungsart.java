package ocp.exercises.e3.not_ocp.zahlungsart;

/**
 * Created by grazi on 29.08.16.
 */
public class Zahlungsart {

    private String typ;

    public Zahlungsart (String typ) {
        this.typ = typ;
    }

    public void fuehreSpezifischeZahlungschritteDurch () {
        if ("Kreditkarte".equals(typ)) {
            System.out.println("....führe Kreditkarten Zahlung aus");
        } else if ("paypal".equals(typ)) {
            System.out.println("....führe paypal Zahlung aus");
        }
    }

    public String getTyp() {
        return typ;
    }
}

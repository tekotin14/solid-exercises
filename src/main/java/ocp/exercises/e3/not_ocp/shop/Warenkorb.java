package ocp.exercises.e3.not_ocp.shop;

import ocp.exercises.e3.not_ocp.zahlungsart.Zahlungsart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 29.08.16.
 */
public class Warenkorb {

    private List<Produkt> produkte = new ArrayList<>();

    public void fuegeProduktHinzu (Produkt produkt) {
        produkte.add(produkt);
    }

    public void entferneProdukt (Produkt produkt) {
        produkte.remove(produkt);
    }

    public double berechneTotal () {
        double total = 0;
        for (Produkt produkt : produkte) {
            total += produkt.getPreis();
        }
        return total;
    }

    public void kaufeProdukte(Zahlungsart zahlungsart) {
        zahlungsart.fuehreSpezifischeZahlungschritteDurch();
        if ("Kreditkarte".equals(zahlungsart.getTyp())) {
            System.out.println("...via kreditkarte gekauft...");
        } else if ("paypal".equals(zahlungsart.getTyp())) {
            System.out.println("...via paypal gekauft...");
        }
    }

}

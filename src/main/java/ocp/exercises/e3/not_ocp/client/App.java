package ocp.exercises.e3.not_ocp.client;

import ocp.exercises.e3.not_ocp.shop.Produkt;
import ocp.exercises.e3.not_ocp.shop.Warenkorb;
import ocp.exercises.e3.not_ocp.zahlungsart.Zahlungsart;

/**
 * Created by grazi on 29.08.16.
 */
public class App {

    public static void main(String[] args) {

        Produkt schuh = new Produkt("Nike Air", 120);
        Produkt kopfhoerer = new Produkt("Panasonic Ear xy", 83.20);
        Warenkorb warenkorb = new Warenkorb();
        warenkorb.fuegeProduktHinzu(schuh);
        warenkorb.fuegeProduktHinzu(kopfhoerer);
        warenkorb.kaufeProdukte(new Zahlungsart("Kreditkarte"));
    }

}

package ocp.exercises.e3.ocp_valid.zahlungsart;

/**
 * Created by grazi on 05.09.16.
 */
public class Kreditkarte extends Zahlungsart {

    public Kreditkarte(String name) {
        super(name);
    }

    @Override
    public void fuehreSpezifischeZahlungschritteDurch() {
        System.out.println("....führe Kreditkarten Zahlung aus");
    }
}

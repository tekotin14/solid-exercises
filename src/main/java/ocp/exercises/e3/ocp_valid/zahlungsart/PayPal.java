package ocp.exercises.e3.ocp_valid.zahlungsart;


/**
 * Created by grazi on 05.09.16.
 */
public class PayPal extends Zahlungsart{

    public PayPal(String name) {
        super(name);
    }

    @Override
    public void fuehreSpezifischeZahlungschritteDurch() {
        System.out.println("....führe paypal Zahlung aus");
    }
}

package ocp.exercises.e3.ocp_valid.zahlungsart;

/**
 * Created by grazi on 29.08.16.
 */
public abstract class Zahlungsart {

    private String name;

    public Zahlungsart (String name) {
        this.name = name;
    }

    public abstract void fuehreSpezifischeZahlungschritteDurch ();

    public String getName() {
        return name;
    }
}

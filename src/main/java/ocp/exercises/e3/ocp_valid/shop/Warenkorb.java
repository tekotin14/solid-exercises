package ocp.exercises.e3.ocp_valid.shop;

import ocp.exercises.e3.ocp_valid.zahlungsart.Zahlungsart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 29.08.16.
 */
public class Warenkorb {

    private List<Produkt> produkte = new ArrayList<>();

    public void fuegeProduktHinzu (Produkt produkt) {
        produkte.add(produkt);
    }

    public void entferneProdukt (Produkt produkt) {
        produkte.remove(produkt);
    }

    public double berechneTotal () {
        double total = 0;
        for (Produkt produkt : produkte) {
            total += produkt.getPreis();
        }
        return total;
    }

    public void kaufeProdukte(Zahlungsart zahlungsart) {
        zahlungsart.fuehreSpezifischeZahlungschritteDurch();
        System.out.println("...via " + zahlungsart.getName() + " gekauft...");

    }


}

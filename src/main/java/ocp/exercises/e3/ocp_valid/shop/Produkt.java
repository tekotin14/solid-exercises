package ocp.exercises.e3.ocp_valid.shop;

/**
 * Created by grazi on 29.08.16.
 */
public class Produkt {

    private String name;
    private double preis;

    public Produkt (String name, double preis) {
        this.name = name;
        this.preis = preis;
    }

    public double getPreis() {
        return preis;
    }

    public String getName() {
        return name;
    }
}

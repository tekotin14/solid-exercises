package ocp.exercises.e3.ocp_valid.client;

import ocp.exercises.e3.ocp_valid.shop.Produkt;
import ocp.exercises.e3.ocp_valid.shop.Warenkorb;
import ocp.exercises.e3.ocp_valid.zahlungsart.PayPal;

/**
 * Created by grazi on 29.08.16.
 */
public class App {

    public static void main(String[] args) {

        Produkt schuh = new Produkt("Nike Air", 120);
        Produkt kopfhoerer = new Produkt("Panasonic Ear xy", 83.20);
        Warenkorb warenkorb = new Warenkorb();
        warenkorb.fuegeProduktHinzu(schuh);
        warenkorb.fuegeProduktHinzu(kopfhoerer);
        warenkorb.kaufeProdukte(new PayPal("Paypal"));
    }

}

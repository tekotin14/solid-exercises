package ocp.exercises.e1.not_ocp.calculating;

import ocp.exercises.e1.not_ocp.shape.Circle;
import ocp.exercises.e1.not_ocp.shape.Retangle;
import ocp.exercises.e1.not_ocp.shape.Shape;

import java.util.List;

/**
 * Created by grazi on 29.08.16.
 */
public class AreaCalculator {

    public double calculateArea (List<Shape> shapes) {
        double area = 0;
        for (Shape shape : shapes) {
            if (shape instanceof Retangle) {
                Retangle retangle = (Retangle) shape;
                area += retangle.getWidth() * retangle.getHeight();
            } else if (shape instanceof Circle) {
                Circle circle = (Circle) shape;
                area += circle.getRadius() * circle.getRadius() * Math.PI;
            }

        }
        return area;
    }

}

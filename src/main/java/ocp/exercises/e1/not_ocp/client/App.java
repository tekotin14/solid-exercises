package ocp.exercises.e1.not_ocp.client;

import ocp.exercises.e1.not_ocp.calculating.AreaCalculator;
import ocp.exercises.e1.not_ocp.shape.Circle;
import ocp.exercises.e1.not_ocp.shape.Retangle;

import java.util.Arrays;

/**
 * Created by grazi on 29.08.16.
 */
public class App {

    public static void main(String[] args) {

        Retangle retangle = new Retangle(12, 23);
        Retangle bigRetangle = new Retangle(200, 120);
        Circle circle = new Circle(4);

        AreaCalculator areaCalculator = new AreaCalculator();

        double calculateArea = areaCalculator.calculateArea(Arrays.asList(retangle, bigRetangle, circle));

        System.out.println("The calculated Area is: " + calculateArea);

    }

}

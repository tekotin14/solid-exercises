package ocp.exercises.e1.not_ocp.shape;

/**
 * Created by grazi on 29.08.16.
 */
public class Circle implements Shape{

    private double radius;

    public Circle (double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
}

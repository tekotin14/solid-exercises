package ocp.exercises.e1.not_ocp.shape;

/**
 * Created by grazi on 29.08.16.
 */
public class Retangle implements Shape {

    private double width;
    private double height;

    public Retangle (double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}

package ocp.exercises.e1.ocp_valid.shape;

/**
 * Created by grazi on 29.08.16.
 */
public class Circle implements Shape {

    private double radius;

    public Circle (double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }
}

package ocp.exercises.e1.ocp_valid.shape;

/**
 * Created by grazi on 29.08.16.
 */
public interface Shape {
    public double getArea ();
}

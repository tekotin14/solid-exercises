package ocp.exercises.e1.ocp_valid.calculating;

import ocp.exercises.e1.ocp_valid.shape.Circle;
import ocp.exercises.e1.ocp_valid.shape.Retangle;
import ocp.exercises.e1.ocp_valid.shape.Shape;

import java.util.List;

/**
 * Created by grazi on 29.08.16.
 */
public class AreaCalculator {

    public double calculateArea (List<Shape> shapes) {
        double area = 0;
        for (Shape shape : shapes) {
            area += shape.getArea();
        }
        return area;
    }

}

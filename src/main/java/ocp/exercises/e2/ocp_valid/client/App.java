package ocp.exercises.e2.ocp_valid.client;

import ocp.exercises.e2.ocp_valid.greeting.ItalianGreeter;

/**
 * Created by grazi on 29.08.16.
 */
public class App {

    public static void main(String[] args) {
        new ItalianGreeter().uebersetzeText();
    }
}

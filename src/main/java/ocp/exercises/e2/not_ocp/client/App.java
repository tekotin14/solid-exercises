package ocp.exercises.e2.not_ocp.client;

import ocp.exercises.e2.not_ocp.greeting.Greeter;
import ocp.exercises.e2.not_ocp.sprache.Sprache;

/**
 * Created by grazi on 29.08.16.
 */
public class App {

    public static void main(String[] args) {
        new Greeter().uebersetzeText(Sprache.ITALIENISCH);
    }
}

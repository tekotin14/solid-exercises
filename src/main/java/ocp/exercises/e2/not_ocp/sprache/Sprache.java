package ocp.exercises.e2.not_ocp.sprache;

/**
 * Created by grazi on 29.08.16.
 */
public enum Sprache {
    DEUTSCH, ITALIENISCH
}

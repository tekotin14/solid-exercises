package ocp.exercises.e2.not_ocp.greeting;

import ocp.exercises.e2.not_ocp.sprache.Sprache;

/**
 * Created by grazi on 29.08.16.
 */
public class Greeter {

    public void uebersetzeText (Sprache sprache) {
        switch (sprache) {
            case DEUTSCH:
                System.out.println("Hallo");
                break;
            case ITALIENISCH:
                System.out.println("Salve");
                break;
            default:
                System.out.println("Sorry, kenne die Sprache: " + sprache + " nicht.");
        }
    }

}

package ocp.ocp_valid;

/**
 * Created by grazi on 30.08.16.
 */
public class Nurse extends Employee {

    public Nurse(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
    }

    // Nurse
    private void checkVitals () {
        System.out.println("... checking vitals");
    }

    private void drawBlood () {
        System.out.println("... drawing blood");
    }

    private void cleanPatientArea ()  {
        System.out.println("... cleaning Patient Area");
    }

    @Override
    public void performDuties() {
        checkVitals();
        drawBlood();
        cleanPatientArea();
    }
}

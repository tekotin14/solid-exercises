package ocp.ocp_valid;

/**
 * Created by grazi on 30.08.16.
 */
public abstract class Employee {

    private int id;
    private String name;
    private String departement;
    private boolean working;

    public Employee(int id, String name, String departement, boolean working) {
        this.id = id;
        this.name = name;
        this.departement = departement;
        this.working = working;
    }

    public abstract void performDuties ();

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDepartement() {
        return departement;
    }

    public boolean isWorking() {
        return working;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", departement='" + departement + '\'' +
                ", working=" + working +
                '}';
    }
}

package ocp.ocp_valid;

/**
 * Created by grazi on 30.08.16.
 */
public class Doctor extends Employee {

    public Doctor(int id, String name, String departement, boolean working) {
        super(id, name, departement, working);
    }

    // Doctor
    private void diagnose () {
        System.out.println("... diagnosing..");
    }

    private void prescribeMeds () {
        System.out.println("... prescribing meds");
    }


    @Override
    public void performDuties() {
        diagnose();
        prescribeMeds();
    }
}

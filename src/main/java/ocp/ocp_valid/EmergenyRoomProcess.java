package ocp.ocp_valid;

/**
 * Created by grazi on 30.08.16.
 */
public class EmergenyRoomProcess {

    public static void main(String[] args) {

        Doctor hans = new Doctor(1, "Hans Muster", "Notfall", true);
        Nurse frida = new Nurse(2, "Frida Tulpe", "Notfall", true);

        HospitalManagement erDirector = new HospitalManagement();

        erDirector.callUpon(hans);
        erDirector.callUpon(frida);
    }

}

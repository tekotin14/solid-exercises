package forming_associations.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class NewsPaperCompany {
    public void startPaperDelivery() {
        // ...
    }

    public void stopPaperDelivery(){
        // ...
    }
}

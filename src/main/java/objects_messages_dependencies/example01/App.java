package objects_messages_dependencies.example01;

/**
 * Created by grazi on 22.08.16.
 */
public class App {
    public static void main(String[] args) {
        Vehicle myCar = new Vehicle();
        Vehicle anotherCar = new Vehicle();

        // Sending message to vehicle Object
        myCar.accelerate();
        anotherCar.accelerate();
    }
}

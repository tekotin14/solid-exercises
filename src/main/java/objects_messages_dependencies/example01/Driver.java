package objects_messages_dependencies.example01;

/**
 * Created by grazi on 22.08.16.
 */
public class Driver {
    public void drive (Vehicle raceCar) {
        raceCar.accelerate();
    }
}

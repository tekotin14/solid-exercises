package objects_messages_dependencies.example02;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 22.08.16.
 */
public class Course {
    private List<Student> registredStudents = new ArrayList<>();
    private String name;
    public Course (String aCourseName) {
        name = aCourseName;
    }
    public void printRegistredStudentsOut () {
        System.out.println("In " + name + " Course:");
        for (Student aStudent: registredStudents) {
            aStudent.printNameOut();
        }
    }
    public void registerStudent (Student aStudent) {
        registredStudents.add(aStudent);
    }
}

package objects_messages_dependencies.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class FrenchCourse extends Course {

    public FrenchCourse() {
        super("French");
    }
}

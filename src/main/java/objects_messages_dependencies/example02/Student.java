package objects_messages_dependencies.example02;

/**
 * Created by grazi on 22.08.16.
 */
public class Student {
    private String name;
    public Student (String aName) {
        name = aName;
    }
    public void printNameOut () {
        System.out.println(name);
    }
}

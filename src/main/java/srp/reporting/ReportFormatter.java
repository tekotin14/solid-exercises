package srp.reporting;


/**
 * Created by grazi on 23.08.16.
 */
public class ReportFormatter {

    private String formattedOutputs;

    public ReportFormatter (Object object, FormatType formatType) {
        switch (formatType) {
            case XML:
                formattedOutputs = convertObjectToXML(object);
                break;
            case CSV:
                formattedOutputs = convertObjectToCSV(object);
                break;
        }
    }

    private String convertObjectToXML (Object object) {
        return "XML : <title> " + object.toString()+ "</title>";
    }

    private String convertObjectToCSV (Object object) {
        return "CSV : ,,,,, " + object.toString()+ ",,,,,";
    }

    protected String getFormattedValue() {
        return formattedOutputs;
    }

}

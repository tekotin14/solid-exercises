package srp.reporting;

/**
 * Created by grazi on 23.08.16.
 */
public enum FormatType {
    XML, CSV
}

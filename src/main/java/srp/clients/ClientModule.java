package srp.clients;

import srp.domain.dao.EmployeeDAO;
import srp.domain.dao.Employee;
import srp.reporting.EmployeeReportFormatter;
import srp.reporting.FormatType;

/**
 * Created by grazi on 23.08.16.
 */
public class ClientModule {

    public static void main(String[] args) {
        Employee peggy = new Employee(1, "Peggy", "accounting", true);
        ClientModule.hirenewEmployee(peggy);
        printEmployeeReport(peggy);
    }

    public static void hirenewEmployee (Employee employee) {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.saveEmployee(employee);
    }

    public static void terminateEmployee (Employee employee) {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.deleteEmployee(employee);
    }
    public static void printEmployeeReport(Employee employee) {
        EmployeeReportFormatter formatter = new EmployeeReportFormatter(employee, FormatType.XML);
        System.out.println(formatter.getFormattedEmployee());;
    }
}
